#!/usr/bin/python
from __future__ import print_function
import os.path


class bsub:
    """
        Class to generate sumission files for bsub
    """

    # Initialise some default values
    def __init__(self, job_string, sh_name='run.sh'):
        # defines the queue
        self.queue = 'slacgpu'
        # sets the wall-time
        self.run_time = '4:00'
        # Necessary RAM in MB
        self.memory = '15000'
        # script path
        self.path = "."
        # defines the name of .sh file
        self.sh_name = sh_name
        # name of log file
        self.log = "run.log"
        # command to execute
        self.job_string = job_string
        # sinularity image to use
        self.singularity_image = "singularity exec --pwd /gpfs/slac/atlas/fs1/u/mg294/workspace/DL1_framework/Training --nv --contain -B /gpfs,/scratch docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/ml-gpu:latest"

    def Path_conv(self, path):
        """
            Converts relative paths to absolute paths.
        """
        return os.path.abspath(path)

    def Write(self):
        """
            Writes the job file
        """
        # check if file already exists, if it does exit since .sh should delete
        # itself after running
        if os.path.exists(self.sh_name):
            print("File already exists, job might have failed!")
            return 0
        sh_file = open(self.sh_name, 'w')
        sh_file.write("#!/bin/bash -l\n\n")

        sh_file.write("# project code  \n")
        sh_file.write("#BSUB -P train_network.py \n\n")

        sh_file.write("# job name  \n")
        sh_file.write("#BSUB -J DL1_training \n\n")

        sh_file.write("# request 1 slots for this job \n")
        sh_file.write("#BSUB -n 1  \n\n")

        sh_file.write("# Job wall clock limit hh:mm \n")
        sh_file.write("#BSUB -W %s  \n\n" % self.run_time)

        sh_file.write("# GPU queue  \n")
        sh_file.write("#BSUB -q %s \n\n" % self.queue)

        sh_file.write("#request gpu's for exclusive use - one per slot (-n)\n")
        sh_file.write("""#BSUB -gpu "num=2:mode=exclusive_process:j_exclusive=no" \n\n""")

        sh_file.write("# set log files (%J is replaced by the job ID)\n")
        sh_file.write("#BSUB -e logs/errors-%J.log\n")
        sh_file.write("#BSUB -o logs/output-%J.log\n")

        sh_file.write(" \n\n")

        # Submit job
        sh_file.write("# Submit Job \n")
        sh_file.write("mkdir logs \n")
        sh_file.write("%s %s" % (self.singularity_image, self.job_string))

        # Remove job file
        sh_file.write("\n\n# Remove submission file \n")
        sh_file.write("rm %s" % self.sh_name)
        return "bsub < %s" % self.sh_name
